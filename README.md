# JWT Service

## Description
A demo JWT service for user authentication control with Spring Security. Service uses a stateless authentication system where necessary user data is stored in JWT's. Information about users and refresh tokens is stored in MonboDB. Service utilizes docker-compose for easier set up.

## Technologies used
- Java 17
- Spring Boot 3.0.6
- Spring Security
- Mongo DB
- Docker compose
- Lombok
