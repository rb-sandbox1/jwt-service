package com.example.jwt.controller;

import com.example.jwt.document.RefreshToken;
import com.example.jwt.document.User;
import com.example.jwt.dto.LoginDTO;
import com.example.jwt.dto.SignupDTO;
import com.example.jwt.dto.TokenDTO;
import com.example.jwt.jwt.JwtHelper;
import com.example.jwt.repository.RefreshTokenRepository;
import com.example.jwt.repository.UserRepository;
import com.example.jwt.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;

    private final RefreshTokenRepository refreshTokenRepository;

    private final JwtHelper jwtHelper;

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final UserService userService;

    @PostMapping("/login")
    @Transactional
    public ResponseEntity<TokenDTO> login(@Valid @RequestBody LoginDTO dto) {
        var auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(auth);
        var user = (User) auth.getPrincipal();
        var refreshToken = new RefreshToken();
        refreshToken.setOwner(user);
        refreshTokenRepository.save(refreshToken);
        var accessToken = jwtHelper.generateAccessToken(user);
        var refreshTokenString = jwtHelper.generateRefreshToken(user, refreshToken.getId());
        return ResponseEntity.ok(new TokenDTO(user.getId(), accessToken, refreshTokenString));
    }

    @PostMapping("/signup")
    @Transactional
    public ResponseEntity<TokenDTO> signup(@Valid @RequestBody SignupDTO dto) {
        var user = new User(dto.getUsername(), dto.getEmail(), passwordEncoder.encode(dto.getPassword()));
        userRepository.save(user);
        var refreshToken = new RefreshToken();
        refreshToken.setOwner(user);
        refreshTokenRepository.save(refreshToken);
        var accessToken = jwtHelper.generateAccessToken(user);
        var refreshTokenString = jwtHelper.generateRefreshToken(user, refreshToken.getId());
        return ResponseEntity.ok(new TokenDTO(user.getId(), accessToken, refreshTokenString));
    }

    @PostMapping("/logout")
    @Transactional
    public ResponseEntity<Void> logout(@Valid @RequestBody TokenDTO dto) {
        var refreshToken = dto.getRefreshToken();
        if (jwtHelper.validateRefreshToken(refreshToken) && refreshTokenRepository.existsById(jwtHelper.getTokenIdFromRefreshToken(refreshToken))) {
            refreshTokenRepository.deleteById(jwtHelper.getTokenIdFromRefreshToken(refreshToken));
            return ResponseEntity.ok().build();
        }
        throw new BadCredentialsException("invalid token");
    }

    @PostMapping("/logoutAll")
    @Transactional
    public ResponseEntity<Void> logoutAll(@Valid @RequestBody TokenDTO dto) {
        var refreshToken = dto.getRefreshToken();
        if (jwtHelper.validateRefreshToken(refreshToken) && refreshTokenRepository.existsById(jwtHelper.getTokenIdFromRefreshToken(refreshToken))) {
            refreshTokenRepository.deleteAllByOwnerId(jwtHelper.getUserIdFromRefreshToken(refreshToken));
            return ResponseEntity.ok().build();
        }
        throw new BadCredentialsException("invalid token");
    }

    @PostMapping("/accessToken")
    @Transactional
    public ResponseEntity<TokenDTO> accessToken(@Valid @RequestBody TokenDTO dto) {
        var refreshToken = dto.getRefreshToken();
        if (jwtHelper.validateRefreshToken(refreshToken) && refreshTokenRepository.existsById(jwtHelper.getTokenIdFromRefreshToken(refreshToken))) {
            var user = (User) userService.findById(jwtHelper.getUserIdFromRefreshToken(refreshToken));
            var accessToken = jwtHelper.generateAccessToken(user);
            return ResponseEntity.ok(new TokenDTO(user.getId(), accessToken, refreshToken));
        }
        throw new BadCredentialsException("invalid token");
    }

    @PostMapping("/refreshToken")
    @Transactional
    public ResponseEntity<TokenDTO> refreshToken(@Valid @RequestBody TokenDTO dto) {
        var refreshTokenString = dto.getRefreshToken();
        if (jwtHelper.validateRefreshToken(refreshTokenString) && refreshTokenRepository.existsById(jwtHelper.getTokenIdFromRefreshToken(refreshTokenString))) {
            var user = (User) userService.findById(jwtHelper.getUserIdFromRefreshToken(refreshTokenString));
            refreshTokenRepository.deleteById(jwtHelper.getTokenIdFromRefreshToken(refreshTokenString));
            var refreshToken = new RefreshToken();
            refreshToken.setOwner(user);
            refreshTokenRepository.save(refreshToken);
            var accessToken = jwtHelper.generateAccessToken(user);
            refreshTokenString = jwtHelper.generateRefreshToken(user, refreshToken.getId());
            return ResponseEntity.ok(new TokenDTO(user.getId(), accessToken, refreshTokenString));
        }
        throw new BadCredentialsException("invalid token");
    }
}
