package com.example.jwt.repository;

import com.example.jwt.document.RefreshToken;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RefreshTokenRepository extends MongoRepository<RefreshToken, String> {

    void deleteAllByOwnerId(String ownerId);
}
