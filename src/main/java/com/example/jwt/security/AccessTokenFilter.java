package com.example.jwt.security;

import com.example.jwt.jwt.JwtHelper;
import com.example.jwt.service.UserService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Optional;

@RequiredArgsConstructor
@Log4j2
@Component
public class AccessTokenFilter extends OncePerRequestFilter {

    private final JwtHelper helper;

    private final UserService service;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            var accessToken = parseAccessToken(request);
            if (accessToken.isPresent() && helper.validateAccessToken(accessToken.get())) {
                var userId = helper.getUserIdFromAccessToken(accessToken.get());
                var user = service.findById(userId);
                var upat = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
                upat.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(upat);
                filterChain.doFilter(request, response);
                return;
            }
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            log.error("cannot set authentication", e);
            filterChain.doFilter(request, response);
        }
    }

    private Optional<String> parseAccessToken(HttpServletRequest request) {
        var authHeader = request.getHeader("Authorization");
        if (StringUtils.hasText(authHeader) && authHeader.startsWith("Bearer ")) {
            return Optional.of(authHeader.replace("Bearer ", ""));
        }
        return Optional.empty();
    }
}
